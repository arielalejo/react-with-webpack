## React without CRA

- Install dependencies
    - `npm i -D @babel/core babel-loader @babel/cli @babel/preset-env @babel/preset-react`
    - `npm i -D webpack webpack-cli webpack-dev-server`
    - `npm i -D html-webpack-plugin`
        * handles and simplifies the creation of HTML files to serve webpack budles
        it generates a single js that will be served along the HTML.
    - `npm i react react-dom`
        * react-dom: serves as the entry point to the DOM and server renderers for React

- Create `public/index.html` with <div id="root"></div>
- Create `src/App.jsx` component
- Create `index.js` with react-dom initializing 

- create `.babelrc`

    ```json
    {
        "presets": ["@babel/preset-env", "@babel/preset-react"]
    }
    ```
    
    * babel will use these packages when transpiling 
    
- configure webpack
    ```js
    const HtmlWebpackPlugin = require("html-webpack-plugin");
    const path = require( "path");

    module.exports = {
        entry: './index.js',
        mode: 'development',
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'index_bundle.js',
        },
        target: 'web',
        devServer: {
            port: '5000',
            static: {
                directory: path.join(__dirname, 'public')
            },
            open: true,
            hot: true,
            liveReload: true,
        },
        resolve: {
            extensions: ['.js', '.jsx', '.json']
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: 'babel-loader'
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: path.join(__dirname, 'public', 'index.html')
            })
        ]
    }
    ```
- add scripts to `package.json`
    ```json
    "start": "webpack-dev-server .",
    "build": "webpack ."
    ```